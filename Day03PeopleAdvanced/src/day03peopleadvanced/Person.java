/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03peopleadvanced;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author oksana
 */
public class Person {

    public Person(String name, int heightCm, Date dateOfBirth) {
        this.name = name;
        this.heightCm = heightCm;
        this.dateOfBirth = dateOfBirth;
    }

    String name;
    int heightCm;
    Date dateOfBirth;

    static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

    @Override
    public String toString() {
        return String.format("%s is %d cm tall, born on %s", name, heightCm, 
                dateFormat.format(dateOfBirth));
    }
}
