package day09firstsql;

import java.sql.*;
import java.util.Scanner;

public class Day09FirstSQL {
    
    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        try {
            Connection conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/ipd23people", "root", "root");
            {
//                System.out.print("Enter name: ");
//                String name = input.nextLine();
//                System.out.print("Enter age: ");
//                int age = input.nextInt();
//
//                String sql = "INSERT INTO people (name, age) VALUES (?, ?)";
//                PreparedStatement statement = conn.prepareStatement(sql);
//                statement.setString(1, name);
//                statement.setInt(2, age);
//
//                int rowsInserted = statement.executeUpdate();
//                if (rowsInserted > 0) {
//                    System.out.println("A new user was inserted successfully!");
//                }
            }
            
            {
                String sql = "SELECT * from people";
                Statement statement = conn.createStatement();
                ResultSet result = statement.executeQuery(sql);
                
                while (result.next()) {
                    int id = result.getInt("id");
                    String name = result.getString("name");
                    int age = result.getInt("age");
                    System.out.printf("Person: id=%d, name=%s, age=%d\n", id, name, age);
                }
            }
            
//            {
//                System.out.println("Enter id of the record to update: ");
//                int id = input.nextInt();
//                input.nextLine();
//                System.out.println("New name: ");
//                String name = input.nextLine();
//                System.out.println("New age: ");
//                int age = input.nextInt();
//                input.nextLine();
//                
//                String sql = "UPDATE people SET name=?, age=?, WHERE id=?";
//                PreparedStatement statement = conn.prepareStatement(sql);
//                statement.setString(1, name);
//                statement.setInt(2, age);
//                statement.setInt(3, id);
//                
//                statement.executeUpdate();
//                System.out.println("Person updated successfully");
//            }
            
            {
                System.out.println("Enter id of the record to delete: ");
                int id = input.nextInt();
                input.nextLine();
                
                String sql = "DELETE FROM people WHERE id=?";
                PreparedStatement statement = conn.prepareStatement(sql);
                statement.setInt(1, id);
                
                int rowsDeleted = statement.executeUpdate();
                if (rowsDeleted > 0) {
                    System.out.println("Success");
                } else {
                    System.out.println("Record not found");
                }
                
//                ResultSet rowsDeleted = statement.executeQuery(sql);
//                if (rowsDeleted == null) {
//                    System.out.println("Record deleted successfully");
//                }
            }
            
        } catch (SQLException ex) {
            System.out.println("Error: " + ex.getMessage());
                ex.printStackTrace();
            }
        }
    }
