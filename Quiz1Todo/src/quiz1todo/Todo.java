package quiz1todo;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Todo {

    public Todo(String task, int difficulty, Date dueDate, String status) {
        this.task = task;
        this.difficulty = difficulty;
        this.dueDate = dueDate;
        this.status = status;
    }

    String task; // 1-100 characters, made up of uppercase and lowercase letters, digits, space, _-(),./\ only
    int difficulty; // 1-5, as slider
    Date dueDate; // year 1900-2100 both inclusive, use formatted field
    String status; // one of: Pending, Done, Delegated - matches the ComboBox in Swing GUI
    
    static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
    
    @Override
    public String toString() {
        return String.format("%s by %tF / diff. %d, %S", task, dueDate, difficulty,  status);
    }
}
