/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day06sorting.Day06Sorting;

import java.util.Comparator;

/**
 *
 * @author oksana
 */
public class Person implements Comparable<Person>{

    public Person(String name, int age, double heightMeters) {
        this.name = name;
        this.age = age;
        this.heightMeters = heightMeters;
    }
    
    String name;
    int age;
    double heightMeters;
    
    @Override
    public String toString(){
        return String.format("Person{%s,%d,%.2f}", name, age, heightMeters);
    }

    @Override
    public int compareTo(Person o) {
        return this.age - o.age;
    }
    
    static final Comparator<Person> compareByNameLambda = (Person p1, Person p2) -> p1.name.compareTo(p2.name);
    
    static final Comparator<Person> compareByNameAnonClass = new Comparator<Person>() {
        @Override
        public int compare(Person p1, Person p2) {
            return p1.name.compareTo(p2.name);
        }
    };
 
    static final Comparator<Person> compareByHeightLambdaShort = (Person p1, Person p2) ->
            p1.heightMeters == p2.heightMeters ? 0 : (p1.heightMeters > p2.heightMeters ? 1 : -1);
    
    static final Comparator<Person> compareByHeightLambda = (Person p1, Person p2) -> {
        if (p1.heightMeters == p2.heightMeters) {
            return 0;
        }
        if (p1.heightMeters > p2.heightMeters) {
            return 1;
        } else {
            return -1;
        }
    };

    static final Comparator<Person> compareByHeightAnonClass = new Comparator<Person>() {
        @Override
        public int compare(Person p1, Person p2) {
            if (p1.heightMeters == p2.heightMeters) {
                return 0;
            }
            if (p1.heightMeters > p2.heightMeters) {
                return 1;
            } else {
                return -1;
            }
        }
    };

    static final Comparator<Person> compareByNameThenAge = new Comparator<Person>() {
        @Override
        public int compare(Person p1, Person p2) {
            int result = p1.name.compareTo(p2.name);
            if (result != 0) {
                return result;
            }
            // names are the same, look at the age
            return p1.age - p2.age;
        }
    };
}
