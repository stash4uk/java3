/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day06sorting.Day06Sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author oksana
 */

public class Day06Sorting {
    static ArrayList<Person> people = new ArrayList<>();
    
    public static void main(String[] args) {
        people.add(new Person("Jerry", 33, 1.67));
        people.add(new Person("Elsa", 22, 1.72));
        people.add(new Person("Adam", 78, 1.88));
        people.add(new Person("Elsa", 15, 1.72));
        people.add(new Person("Martha", 55, 1.92));
        people.add(new Person("Elsa", 25, 1.72));
        people.add(new Person("Tom", 31, 1.77));
        people.add(new Person("Elsa", 21, 1.72));
        people.add(new Person("Barry", 53, 1.82));
        System.out.println("Original order");
        System.out.println(Arrays.deepToString(people.toArray()));
        
        Collections.sort(people);
        System.out.println("Ordered by age");
        System.out.println(Arrays.deepToString(people.toArray()));
        
        Collections.sort(people, Person.compareByNameLambda);
        System.out.println("Ordered by name (using Comparator)");
        System.out.println(Arrays.deepToString(people.toArray()));
        
        Collections.sort(people, Person.compareByHeightAnonClass);
        System.out.println("Ordered by height (using Comparator)");
        System.out.println(Arrays.deepToString(people.toArray()));

        Collections.sort(people, Person.compareByNameThenAge);
        System.out.println("Ordered by name then age (using Comparator)");
        System.out.println(Arrays.deepToString(people.toArray()));
    }
}



class ComparePersonByName implements Comparator<Person> {

    @Override
    public int compare(Person p1, Person p2) {
        return p1.name.compareTo(p2.name);
    }
    
}

class ComparePersonByHeight implements Comparator<Person> {

    @Override
    public int compare(Person p1, Person p2) {
        if(p1.heightMeters < p2.heightMeters) {
            return -1;
        }    
        else if(p1.heightMeters > p2.heightMeters) {
            return 0;
        }
        else {
            return 0;
        }
    }
    
}