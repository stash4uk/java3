package day09todoappdb;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Todo {

    public Todo(int id, String todo, int difficulty, Date dueDate, Status status) {
        this.id = id; // setId(id) = id with setters
        this.todo = todo; // setTask(todo)= todo
        this.difficulty = difficulty;
        this.dueDate = dueDate;
        this.status = status;
    }

    int id; // private int id; with setters
    String todo; // 1-100 characters (matching varchar length!), any characters are allowed
    int difficulty; // 1-5
    Date dueDate; // any valid date where year is 1970 to 2100 both inclusive
    Status status;

    enum Status {
        Pending, Done, Delegated
    }
    
    static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    
    @Override
    public String toString() {
        return String.format("%d: %s, diff: %d, due on: %s, %s", 
                id, todo, difficulty, dateFormat.format(dueDate), status);
    }
}
