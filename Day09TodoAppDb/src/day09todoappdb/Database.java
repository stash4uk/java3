/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day09todoappdb;

import day09todoappdb.Todo.Status;
import static day09todoappdb.Todo.dateFormat;
import java.sql.*;
import java.util.ArrayList;

public class Database {

    Connection conn;
    java.util.Date dueDate = new java.util.Date();
    java.sql.Date sqlDate = new java.sql.Date(dueDate.getTime());

    public Database() throws SQLException {
        conn = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/ipd23todoapp", "root", "root");
    }
    
    public int addTodo(Todo t) throws SQLException {
        String sql = "INSERT INTO todos (todo, difficulty, dueDate, status) VALUES (?, ?, ?, ?)";
        PreparedStatement statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, t.todo);
        statement.setInt(2, t.difficulty);
        statement.setDate(3, sqlDate);
        statement.setString(4, t.status.toString());

        statement.executeUpdate();

        ResultSet rs = statement.getGeneratedKeys();
        if (rs.next()) {
            int lastInsertedId = rs.getInt(1);
            return lastInsertedId;

        }
        throw new SQLException("Id after insert not found");
    }
    
    public ArrayList<Todo> getAllTodos() throws SQLException {
        ArrayList<Todo> list = new ArrayList<>();
        String sql = "SELECT * from todos";
        Statement statement = conn.createStatement();
        ResultSet result = statement.executeQuery(sql);

        while (result.next()) {
            int id = result.getInt("id");
            String todo = result.getString("todo");
            int difficulty = result.getInt("difficulty");
            Date dueDate = result.getDate("dueDate");
            Status status = Status.valueOf("status");
            list.add(new Todo(id, todo, difficulty, dueDate, status));
        }
        return list;
    }
    
    
}
