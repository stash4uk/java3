
package day07custdialogfriends;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.Date;

public class Birthday {

    public Birthday(String name, Date birthday) {
        this.name = name;
        this.birthday = birthday;
    }
    String name;
    Date birthday;

    public void setName(String name) {
        this.name = name;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public Date getBirthday() {
        return birthday;
    }
    
    public int getDaysTillBday() {
        
        LocalDate today = LocalDate.now();
        LocalDate birthday = LocalDate.of(1960, Month.JANUARY, 1);

        LocalDate nextBDay = birthday.withYear(today.getYear());

        //If your birthday has occurred this year already, add 1 to the year.
        if (nextBDay.isBefore(today) || nextBDay.isEqual(today)) {
            nextBDay = nextBDay.plusYears(1);
        }

        Period p = Period.between(today, nextBDay);
        long p2 = ChronoUnit.DAYS.between(today, nextBDay);
        return 0;
    }
    
    static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
    
    @Override
    public String toString() {
        return String.format("%s born %s birthday in %d days", 
                name, dateFormat.format(birthday), getDaysTillBday());
    }
    
    static final Comparator<Birthday> compareByName = (Birthday f1, Birthday f2)
            -> f1.name.compareTo(f2.name);
}
