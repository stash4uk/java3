
package quiz3booksandauthors;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
//import java.sql.InvalidDataException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.Blob;
import java.sql.Types;
import javax.sql.rowset.serial.SerialBlob;
import java.util.Date;


public class Database {
    private Connection conn;

    public Database() throws SQLException {
        conn = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/quiz3booksandauthors", "root", "root");
    }
    
     public ArrayList<String> getAllBooks() throws SQLException {

        ArrayList<String> resultList = new ArrayList<>();

        try {

            String sql = "SELECT * FROM books";
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery(sql); //SQLException
            while (result.next()) {

                int id = result.getInt("id");
                String name = result.getString("name");

                resultList.add(id, name);

            }

        } catch (InvalidDataExeption ex) {
            throw new SQLException("Error getting books from database", ex.getMessage());
        }
        return resultList;
    }
     
     public int addAuthor(Author a) throws SQLException {

        String sql = "INSERT INTO authors (name) VALUES (?)";

        PreparedStatement statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

        statement.setString(1, a.getName());
        statement.executeUpdate();  //SQLException

        ResultSet rs = statement.getGeneratedKeys();

        if (rs.next()) {
            int lastInsertedId = rs.getInt(1);
            return lastInsertedId;
        }

        throw new SQLException("Id after insert not found");
    }
    
    public int addBook(Book b) throws SQLException {

        String sql = "INSERT INTO books (title, isbn, pubDate, coverImg) VALUES (?, ?, ?, ?)";

        PreparedStatement statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

        statement.setString(1, b.getTitle());
        statement.setString(2, b.getIsbn());
        statement.setDate(3, new java.sql.Date(b.getPubDate().getTime()));
        Blob coverImg = new SerialBlob(b.getCoverImg());
        statement.setBlob(4, coverImg);
        
        statement.executeUpdate();  //SQLException

        ResultSet rs = statement.getGeneratedKeys();

        if (rs.next()) {
            int lastInsertedId = rs.getInt(1);
            return lastInsertedId;
        }

        throw new SQLException("Id after insert not found");
    }
}

