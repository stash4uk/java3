package quiz3booksandauthors;

import java.util.ArrayList;
import java.util.Date;

public class Book {

    private int id; // INT PK AI
    private String title; // VC(100), verify 1-100 chars long
    private String isbn; // VC(13) UNIQ, allow ISBN, regex: 13 character format - the first 12 are only 0-9, then last one is 0-9 or X, ignore dashes
    private Date pubDate; // DATE, any date is okay
    private byte[] coverImg; // LARGEBLOB, no NULL allowed (picture must be assigned)
    private ArrayList<String> authorList ; // not a field in the database but may help your implementation if you want to use it

    public Book(String title, String isbn, Date pubDate, byte[] coverImg, ArrayList<String> authorList) {
        setId(id);
        setTitle(title);
        setIsbn(isbn);
        setPubDate(pubDate);
        setCoverImg(coverImg);
        setAuthorList(authorList);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if (!title.matches("[.]{1,100}")) {
            throw new IllegalArgumentException("Title must be 1-100 characters long");
        }
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        if (!isbn.matches("([0-9][0-9 | X]){13,13}")) {
            throw new IllegalArgumentException("ISBN must be 13 characters long");
        }
        this.isbn = isbn;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }

    public byte[] getCoverImg() {
        return coverImg;
    }

    public void setCoverImg(byte[] coverImg) {
        this.coverImg = coverImg;
    }

    public ArrayList<String> getAuthorList() {
        return authorList;
    }

    public void setAuthorList(ArrayList<String> authorList) {
        this.authorList = authorList;
    }

    @Override
    public String toString() {
        return String.format("%d %s by %s", id, title, authorList);
    }
    
    
    
}
