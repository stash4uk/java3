package quiz3booksandauthors;

public class Author {

    private int id; // INT PK AI
    private int bookId; // INT, FK -> books.id
    private String name; // VC(50), verify 1-50 chars long

    public Author(int id, int bookId, String name) {
        setId(id);
        setBookId(bookId);
        setName(name);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (!name.matches(".{1,50}")) {
            throw new IllegalArgumentException("Title must be 1-50 characters long");
        }
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%d %s", id, name);
    }

}
