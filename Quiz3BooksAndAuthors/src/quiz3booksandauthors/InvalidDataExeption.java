package quiz3booksandauthors;

public class InvalidDataExeption extends Exception {

    public InvalidDataExeption(String msg) {
        super(msg);
    }
}
