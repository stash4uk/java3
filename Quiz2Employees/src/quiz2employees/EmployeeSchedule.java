/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz2employees;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;

enum Weekday {
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Sunday,
    }

public class EmployeeSchedule {

    public EmployeeSchedule(String name, boolean isManager, String department, Date dateHired) {
        this.name = name;
        this.isManager = isManager;
        this.department = department;
        this.dateHired = dateHired;
    }

    public EmployeeSchedule(String dataLine) {
        
    }
    
  String name; // 2-50 characters, not permitted are: ;^?@!~*
  boolean isManager;
  String position = "";
  String department; // 2-50 characters, not permitted are: ;^?@!~*
  Date dateHired; // year between 1900 and 2100
  HashSet<Weekday> workdaysList = new HashSet<>(); // no duplicates allowed
  String dataLine;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Date getDateHired() {
        return dateHired;
    }

    public void setDateHired(Date dateHired) {
        this.dateHired = dateHired;
    }
    
    public String toDataString() {
        return dataLine;
    }
    
    static SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy");
    
    

    @Override
    public String toString() {
        return String.format(name + ", " + isManager + " of " + department + ", hired on " + 
                dateFormat.format(dateHired));
    }
  
    
  
}

