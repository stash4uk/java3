/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day02fileio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author oksana
 */
public class Day02FileIO {
    
    public static void main(String[] args) throws FileNotFoundException {
        
        // INPUT USER NAME
        Scanner userInput = new Scanner(System.in); // declare and initialize scanner
	System.out.print("Enter your name: ");        
        String userName = userInput.nextLine();
        int count = (int) (Math.random() * 10 + 1);       
        
    
        try (PrintWriter fileOutput = new PrintWriter(new File("C:\\Users\\oksana\\Desktop\\JAC\\Java III\\Day02\\output.txt"))) {
            for (int i = 0; i < count; i++) {
                fileOutput.println(userName);
            }
        } catch (IOException ex) {
            System.out.println("Error writing to file: " + ex.getMessage());
        }
        
        try (Scanner fileInput = new Scanner(new File("C:\\Users\\oksana\\Desktop\\JAC\\Java III\\Day02\\output.txt"))){
            while (fileInput.hasNextLine()) {
                String line = fileInput.nextLine();
                System.out.println("Line: " + line);
            }
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }
                               
               
    }
}
